/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applaboratoriouno;

import applaboratoriouno.Posiciones.Equipo;
import static applaboratoriouno.Posiciones.Equipo.imprimirArrayEquipos;
import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author PC
 */
public class AppLaboratorioUno {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int gfs = 0;int gfa = 0;int gfh = 0;int gfc = 0;int gfg = 0;int gfl = 0;
        int gcs = 0;int gca = 0;int gch = 0;int gcc = 0;int gcg = 0;int gcl = 0;
        int ps = 0; int pa = 0; int ph = 0; int pc = 0; int pg = 0; int pl = 0;
        Jugar oJugar = new Jugar();
        Scanner var = new Scanner(System.in);
        int opcion_principal = 0;
        boolean continuar = true;
        while(continuar){
            
            System.out.println("\nBienvenido(a) Usuario." + "\n" + 
                    "Por favor seleccione una opcion: " + "\n" + "1- Jugar." + "\n" + "2- Tabla de Posiciones." + "\n" + "3- Reporte." + "\n" + "4- Salir." + "\n");
            opcion_principal = var.nextInt();
            
            if(opcion_principal < 1 || opcion_principal > 4){
                
                System.out.println("Opcion no disponible." + "\n");
                continuar = true;
                
            }else if (opcion_principal == 1){
                
                String[] equipos = oJugar.generar();
                System.out.println("Partidos:\n" + equipos[0] + " vs " + equipos[1] + "\n" + equipos[2] + " vs " + equipos[3] + "\n" + equipos[4] + " vs " + equipos[5] + "\n");
                String[] p11 = oJugar.realizarPartido(equipos[0], equipos[1]);
                String[] p2 = oJugar.realizarPartido(equipos[2], equipos[3]);
                String[] p3 = oJugar.realizarPartido(equipos[4], equipos[5]);
                
                int tamano1 = p11.length;
                int tamano2 = p2.length;
                int tamano3 = p3.length;
                
                if (tamano1 == 6) {
                   
                    if (p11[0] == "Saprissa") {
                        ps = ps + Integer.parseInt(p11[2]);
                        gfs = gfs + Integer.parseInt(p11[1]);
                        gcs = gcs + Integer.parseInt(p11[4]);
                        
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");

                    }
                    if (p11[3] == "Saprissa") {
                        ps = ps + Integer.parseInt(p11[5]);
                        gfs = gfs + Integer.parseInt(p11[4]);
                        gcs = gcs + Integer.parseInt(p11[1]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");
                    }
                    if (p11[0] == "Alajuelense") {
                        pa = pa + Integer.parseInt(p11[2]);
                        gfa = gfa + Integer.parseInt(p11[1]);
                        gca = gca + Integer.parseInt(p11[4]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");

                    }
                    if (p11[3] == "Alajuelense") {
                        pa = pa + Integer.parseInt(p11[5]);
                        gfa = gfa + Integer.parseInt(p11[4]);
                        gca = gca + Integer.parseInt(p11[1]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");
                    }
                    if (p11[0] == "Herediano") {
                        ph = ph + Integer.parseInt(p11[2]);
                        gfh = gfh + Integer.parseInt(p11[1]);
                        gch = gch + Integer.parseInt(p11[4]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");

                    }
                    if (p11[3] == "Herediano") {
                        ph = ph + Integer.parseInt(p11[5]);
                        gfh = gfh + Integer.parseInt(p11[4]);
                        gch = gch + Integer.parseInt(p11[1]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");
                    }
                    if (p11[0] == "Cartago") {
                        pc = pc + Integer.parseInt(p11[2]);
                        gfc = gfc + Integer.parseInt(p11[1]);
                        gcc = gcc + Integer.parseInt(p11[4]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");

                    }
                    if (p11[3] == "Cartago") {
                        pc = pc + Integer.parseInt(p11[5]);
                        gfc = gfc + Integer.parseInt(p11[4]);
                        gcc = gcc + Integer.parseInt(p11[1]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");
                    }
                    if (p11[0] == "Guadalupe") {
                        pg = pg + Integer.parseInt(p11[2]);
                        gfg = gfg + Integer.parseInt(p11[1]);
                        gcg = gcg + Integer.parseInt(p11[4]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");

                    }
                    if (p11[3] == "Guadalupe") {
                        pg = pg + Integer.parseInt(p11[5]);
                        gfg = gfg + Integer.parseInt(p11[4]);
                        gcg = gcg + Integer.parseInt(p11[1]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");
                    }
                    if (p11[0] == "Limon") {
                        pl = pl + Integer.parseInt(p11[2]);
                        gfl = gfl + Integer.parseInt(p11[1]);
                        gcl = gcl + Integer.parseInt(p11[4]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");

                    }
                    if (p11[3] == "Limon") {
                        pl = pl + Integer.parseInt(p11[5]);
                        gfl = gfl + Integer.parseInt(p11[4]);
                        gcl = gcl + Integer.parseInt(p11[1]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");
                    }
                }   
                    ///////////////////////////////////////////////
                if(tamano2 == 6){    
                    if (p2[0] == "Saprissa") {
                        ps = ps + Integer.parseInt(p2[2]);
                        gfs = gfs + Integer.parseInt(p2[1]);
                        gcs = gcs + Integer.parseInt(p2[4]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");

                    }
                    if (p2[3] == "Saprissa") {
                        ps = ps + Integer.parseInt(p2[5]);
                        gfs = gfs + Integer.parseInt(p2[4]);
                        gcs = gcs + Integer.parseInt(p2[1]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");
                    }
                    if (p2[0] == "Alajuelense") {
                        pa = pa + Integer.parseInt(p2[2]);
                        gfa = gfa + Integer.parseInt(p2[1]);
                        gca = gca + Integer.parseInt(p2[4]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");

                    }
                    if (p2[3] == "Alajuelense") {
                        pa = pa + Integer.parseInt(p2[5]);
                        gfa = gfa + Integer.parseInt(p2[4]);
                        gca = gca + Integer.parseInt(p2[1]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");
                    }
                    if (p2[0] == "Herediano") {
                        ph = ph + Integer.parseInt(p2[2]);
                        gfh = gfh + Integer.parseInt(p2[1]);
                        gch = gch + Integer.parseInt(p2[4]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");

                    }
                    if (p2[3] == "Herediano") {
                        ph = ph + Integer.parseInt(p2[5]);
                        gfh = gfh + Integer.parseInt(p2[4]);
                        gch = gch + Integer.parseInt(p2[1]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");
                    }
                    if (p2[0] == "Cartago") {
                        pc = pc + Integer.parseInt(p2[2]);
                        gfc = gfc + Integer.parseInt(p2[1]);
                        gcc = gcc + Integer.parseInt(p2[4]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");

                    }
                    if (p2[3] == "Cartago") {
                        pc = pc + Integer.parseInt(p2[5]);
                        gfc = gfc + Integer.parseInt(p2[4]);
                        gcc = gcc + Integer.parseInt(p2[1]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");
                    }
                    if (p2[0] == "Guadalupe") {
                        pg = pg + Integer.parseInt(p2[2]);
                        gfg = gfg + Integer.parseInt(p2[1]);
                        gcg = gcg + Integer.parseInt(p2[4]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");

                    }
                    if (p2[3] == "Guadalupe") {
                        pg = pg + Integer.parseInt(p2[5]);
                        gfg = gfg + Integer.parseInt(p2[4]);
                        gcg = gcg + Integer.parseInt(p2[1]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");
                    }
                    if (p2[0] == "Limon") {
                        pl = pl + Integer.parseInt(p2[2]);
                        gfl = gfl + Integer.parseInt(p2[1]);
                        gcl = gcl + Integer.parseInt(p2[4]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");

                    }
                    if (p2[3] == "Limon") {
                        pl = pl + Integer.parseInt(p2[5]);
                        gfl = gfl + Integer.parseInt(p2[4]);
                        gcl = gcl + Integer.parseInt(p2[1]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");
                    }
                    
                }    ///////////////////////////////////////
                
                if(tamano3 == 6){
                    
                    if (p3[0] == "Saprissa") {
                        ps = ps + Integer.parseInt(p3[2]);
                        gfs = gfs + Integer.parseInt(p3[1]);
                        gcs = gcs + Integer.parseInt(p3[4]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");

                    }
                    if (p3[3] == "Saprissa") {
                        ps = ps + Integer.parseInt(p3[5]);
                        gfs = gfs + Integer.parseInt(p3[4]);
                        gcs = gcs + Integer.parseInt(p3[1]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");
                    }
                    if (p3[0] == "Alajuelense") {
                        pa = pa + Integer.parseInt(p3[2]);
                        gfa = gfa + Integer.parseInt(p3[1]);
                        gca = gca + Integer.parseInt(p3[4]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");

                    }
                    if (p3[3] == "Alajuelense") {
                        pa = pa + Integer.parseInt(p3[5]);
                        gfa = gfa + Integer.parseInt(p3[4]);
                        gca = gca + Integer.parseInt(p3[1]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");
                    }
                    if (p3[0] == "Herediano") {
                        ph = ph + Integer.parseInt(p3[2]);
                        gfh = gfh + Integer.parseInt(p3[1]);
                        gch = gch + Integer.parseInt(p3[4]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");

                    }
                    if (p3[3] == "Herediano") {
                        ph = ph + Integer.parseInt(p3[5]);
                        gfh = gfh + Integer.parseInt(p3[4]);
                        gch = gch + Integer.parseInt(p3[1]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");
                    }
                    if (p3[0] == "Cartago") {
                        pc = pc + Integer.parseInt(p3[2]);
                        gfc = gfc + Integer.parseInt(p3[1]);
                        gcc = gcc + Integer.parseInt(p3[4]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");

                    }
                    if (p3[3] == "Cartago") {
                        pc = pc + Integer.parseInt(p3[5]);
                        gfc = gfc + Integer.parseInt(p3[4]);
                        gcc = gcc + Integer.parseInt(p3[1]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");
                    }
                    if (p3[0] == "Guadalupe") {
                        pg = pg + Integer.parseInt(p3[2]);
                        gfg = gfg + Integer.parseInt(p3[1]);
                        gcg = gcg + Integer.parseInt(p3[4]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");

                    }
                    if (p3[3] == "Guadalupe") {
                        pg = pg + Integer.parseInt(p3[5]);
                        gfg = gfg + Integer.parseInt(p3[4]);
                        gcg = gcg + Integer.parseInt(p3[1]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");
                    }
                    if (p3[0] == "Limon") {
                        pl = pl + Integer.parseInt(p3[2]);
                        gfl = gfl + Integer.parseInt(p3[1]);
                        gcl = gcl + Integer.parseInt(p3[4]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");

                    }
                    if (p3[3] == "Limon") {
                        pl = pl + Integer.parseInt(p3[5]);
                        gfl = gfl + Integer.parseInt(p3[4]);
                        gcl = gcl + Integer.parseInt(p3[1]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");
                    }
                }
                
                if(tamano1 == 8){
                    if (p11[0] == "Saprissa") {
                        ps = ps + Integer.parseInt(p11[2]);
                        gfs = gfs + Integer.parseInt(p11[1]);
                        gcs = gcs + Integer.parseInt(p11[5]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");

                    }
                    if (p11[4] == "Saprissa") {
                        ps = ps + Integer.parseInt(p11[6]);
                        gfs = gfs + Integer.parseInt(p11[5]);
                        gcs = gcs + Integer.parseInt(p11[1]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");
                    }
                    if (p11[0] == "Alajuelense") {
                        pa = pa + Integer.parseInt(p11[2]);
                        gfa = gfa + Integer.parseInt(p11[1]);
                        gca = gca + Integer.parseInt(p11[5]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");

                    }
                    if (p11[4] == "Alajuelense") {
                        pa = pa + Integer.parseInt(p11[6]);
                        gfa = gfa + Integer.parseInt(p11[5]);
                        gca = gca + Integer.parseInt(p11[1]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");
                    }
                    if (p11[0] == "Herediano") {
                        ph = ph + Integer.parseInt(p11[2]);
                        gfh = gfh + Integer.parseInt(p11[1]);
                        gch = gch + Integer.parseInt(p11[5]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");

                    }
                    if (p11[4] == "Herediano") {
                        ph = ph + Integer.parseInt(p11[6]);
                        gfh = gfh + Integer.parseInt(p11[5]);
                        gch = gch + Integer.parseInt(p11[1]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");
                    }
                    if (p11[0] == "Cartago") {
                        pc = pc + Integer.parseInt(p11[2]);
                        gfc = gfc + Integer.parseInt(p11[1]);
                        gcc = gcc + Integer.parseInt(p11[5]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");

                    }
                    if (p11[4] == "Cartago") {
                        pc = pc + Integer.parseInt(p11[6]);
                        gfc = gfc + Integer.parseInt(p11[5]);
                        gcc = gcc + Integer.parseInt(p11[1]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");
                    }
                    if (p11[4] == "Guadalupe") {
                        pg = pg + Integer.parseInt(p11[2]);
                        gfg = gfg + Integer.parseInt(p11[1]);
                        gcg = gcg + Integer.parseInt(p11[5]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");

                    }
                    if (p11[4] == "Guadalupe") {
                        pg = pg + Integer.parseInt(p11[6]);
                        gfg = gfg + Integer.parseInt(p11[5]);
                        gcg = gcg + Integer.parseInt(p11[1]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");
                    }
                    if (p11[0] == "Limon") {
                        pl = pl + Integer.parseInt(p11[2]);
                        gfl = gfl + Integer.parseInt(p11[1]);
                        gcl = gcl + Integer.parseInt(p11[5]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");

                    }
                    if (p11[4] == "Limon") {
                        pl = pl + Integer.parseInt(p11[6]);
                        gfl = gfl + Integer.parseInt(p11[5]);
                        gcl = gcl + Integer.parseInt(p11[1]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");
                    }
                    
                }    ////////////////////////////
                
                if(tamano2 == 8){
                    if (p2[0] == "Saprissa") {
                        ps = ps + Integer.parseInt(p2[2]);
                        gfs = gfs + Integer.parseInt(p2[1]);
                        gcs = gcs + Integer.parseInt(p2[5]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");

                    }
                    if (p2[4] == "Saprissa") {
                        ps = ps + Integer.parseInt(p2[6]);
                        gfs = gfs + Integer.parseInt(p2[5]);
                        gcs = gcs + Integer.parseInt(p2[1]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");
                    }
                    if (p2[0] == "Alajuelense") {
                        pa = pa + Integer.parseInt(p2[2]);
                        gfa = gfa + Integer.parseInt(p2[1]);
                        gca = gca + Integer.parseInt(p2[5]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");

                    }
                    if (p2[4] == "Alajuelense") {
                        pa = pa + Integer.parseInt(p2[6]);
                        gfa = gfa + Integer.parseInt(p2[5]);
                        gca = gca + Integer.parseInt(p2[1]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");
                    }
                    if (p2[0] == "Herediano") {
                        ph = ph + Integer.parseInt(p2[2]);
                        gfh = gfh + Integer.parseInt(p2[1]);
                        gch = gch + Integer.parseInt(p2[5]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");

                    }
                    if (p2[4] == "Herediano") {
                        ph = ph + Integer.parseInt(p2[6]);
                        gfh = gfh + Integer.parseInt(p2[5]);
                        gch = gch + Integer.parseInt(p2[1]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");
                    }
                    if (p2[0] == "Cartago") {
                        pc = pc + Integer.parseInt(p2[2]);
                        gfc = gfc + Integer.parseInt(p2[1]);
                        gcc = gcc + Integer.parseInt(p2[5]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");

                    }
                    if (p2[4] == "Cartago") {
                        pc = pc + Integer.parseInt(p2[6]);
                        gfc = gfc + Integer.parseInt(p2[5]);
                        gcc = gcc + Integer.parseInt(p2[1]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");
                    }
                    if (p2[0] == "Guadalupe") {
                        pg = pg + Integer.parseInt(p2[2]);
                        gfg = gfg + Integer.parseInt(p2[1]);
                        gcg = gcg + Integer.parseInt(p2[5]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");

                    }
                    if (p2[4] == "Guadalupe") {
                        pg = pg + Integer.parseInt(p2[6]);
                        gfg = gfg + Integer.parseInt(p2[5]);
                        gcg = gcg + Integer.parseInt(p2[1]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");
                    }
                    if (p2[0] == "Limon") {
                        pl = pl + Integer.parseInt(p2[2]);
                        gfl = gfl + Integer.parseInt(p2[1]);
                        gcl = gcl + Integer.parseInt(p2[5]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");

                    }
                    if (p2[4] == "Limon") {
                        pl = pl + Integer.parseInt(p2[6]);
                        gfl = gfl + Integer.parseInt(p2[5]);
                        gcl = gcl + Integer.parseInt(p2[1]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");
                    }
                    
                    ////////////////////////////
                }
                if(tamano3 == 8){
                    if (p3[0] == "Saprissa") {
                        ps = ps + Integer.parseInt(p3[2]);
                        gfs = gfs + Integer.parseInt(p3[1]);
                        gcs = gcs + Integer.parseInt(p3[5]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");

                    }
                    if (p3[4] == "Saprissa") {
                        ps = ps + Integer.parseInt(p3[6]);
                        gfs = gfs + Integer.parseInt(p3[5]);
                        gcs = gcs + Integer.parseInt(p3[1]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");
                    }
                    if (p3[0] == "Alajuelense") {
                        pa = pa + Integer.parseInt(p3[2]);
                        gfa = gfa + Integer.parseInt(p3[1]);
                        gca = gca + Integer.parseInt(p3[5]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");

                    }
                    if (p3[4] == "Alajuelense") {
                        pa = pa + Integer.parseInt(p3[6]);
                        gfa = gfa + Integer.parseInt(p3[5]);
                        gca = gca + Integer.parseInt(p3[1]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");
                    }
                    if (p3[0] == "Herediano") {
                        ph = ph + Integer.parseInt(p3[2]);
                        gfh = gfh + Integer.parseInt(p3[1]);
                        gch = gch + Integer.parseInt(p3[5]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");

                    }
                    if (p3[4] == "Herediano") {
                        ph = ph + Integer.parseInt(p3[6]);
                        gfh = gfh + Integer.parseInt(p3[5]);
                        gch = gch + Integer.parseInt(p3[1]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");
                    }
                    if (p3[0] == "Cartago") {
                        pc = pc + Integer.parseInt(p3[2]);
                        gfc = gfc + Integer.parseInt(p3[1]);
                        gcc = gcc + Integer.parseInt(p3[5]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");

                    }
                    if (p3[4] == "Cartago") {
                        pc = pc + Integer.parseInt(p3[6]);
                        gfc = gfc + Integer.parseInt(p3[5]);
                        gcc = gcc + Integer.parseInt(p3[1]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");
                    }
                    if (p3[0] == "Guadalupe") {
                        pg = pg + Integer.parseInt(p3[2]);
                        gfg = gfg + Integer.parseInt(p3[1]);
                        gcg = gcg + Integer.parseInt(p3[5]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");

                    }
                    if (p3[4] == "Guadalupe") {
                        pg = pg + Integer.parseInt(p3[6]);
                        gfg = gfg + Integer.parseInt(p3[5]);
                        gcg = gcg + Integer.parseInt(p3[1]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");
                    }
                    if (p3[0] == "Limon") {
                        pl = pl + Integer.parseInt(p3[2]);
                        gfl = gfl + Integer.parseInt(p3[1]);
                        gcl = gcl + Integer.parseInt(p3[5]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");

                    }
                    if (p3[4] == "Limon") {
                        pl = pl + Integer.parseInt(p3[6]);
                        gfl = gfl + Integer.parseInt(p3[5]);
                        gcl = gcl + Integer.parseInt(p3[1]);
                        //System.out.println("Saprissa" + " | " + ps + " | " + gfs + " | " + gcs + "\n");
                    }
                }
                
                
                
                
                continuar = true;
            }else if(opcion_principal == 2){
                
                Equipo[] arrayEquipos = new Equipo[6];
                arrayEquipos[0] = new Equipo("SAP", ps, gfs, gcs);
                arrayEquipos[1] = new Equipo("LDA", pa, gfa, gca);
                arrayEquipos[2] = new Equipo("CSH", ph, gfh, gch);
                arrayEquipos[3] = new Equipo("CSC", pc, gfc, gcc);
                arrayEquipos[4] = new Equipo("LIM", pl, gfl, gcl);
                arrayEquipos[5] = new Equipo("GUA", pg, gfg, gcg);

                
                Arrays.sort(arrayEquipos);
                System.out.println("POS| EQUIPO     |PTS|GFA|GCO|");
                imprimirArrayEquipos(arrayEquipos);
                
                continuar = true;
            }else if(opcion_principal == 3){
                oJugar.imprimirGanes();
                
                continuar = true;
            }else if (opcion_principal == 4){
                continuar = false;
            }
                
            
        }
    }
    
}
